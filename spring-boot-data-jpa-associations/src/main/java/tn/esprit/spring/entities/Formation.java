package tn.esprit.spring.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "T_FORMATION")
public class Formation implements Serializable {
 	 
	private static final long serialVersionUID = 1L;
 
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="FORMATION_ID") 
	private Long id; // Identifiant formation (Clé primaire)
	  
	@Column(name="FORMATION_THEME")
	private String theme; // Thème formation 
	   
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="FK_FD_ID")
	private FormationDetail formationDetail; 
	  
	@ManyToMany(cascade = CascadeType.ALL) 
	@JoinTable(name = "T_FORMATION_TP", 
	joinColumns = { @JoinColumn(name = "FORMATION_ID") }, 
	inverseJoinColumns = { @JoinColumn(name = "TP_ID") })
	private Set<TravauxPratiques> formationTps;  
	// if Set is not working, then use List / ArrayList : 
	// private List<TravauxPratiques> formationTps;	
	 
	public Formation() {}
	public Formation(String theme, FormationDetail formationDetail) {
		this.theme = theme;
		this.formationDetail = formationDetail;
	}
	public Formation(String theme) { this.theme = theme; }	
	public Formation(Long id, String theme) { this.id = id; this.theme = theme; }	
	public Formation(Long id) { this.id = id; }
//	public Formation(String theme, ArrayList<TravauxPratiques> formationTps) { this.theme = theme; this.formationTps = formationTps; }
//	public Formation(String theme, ArrayList<TravauxPratiques> formationTps, FormationDetail formationDetail) {
//		this.theme = theme;
//		this.formationTps = formationTps;
//		this.formationDetail = formationDetail;
//	}
	 
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	
	public String getTheme() { return theme; }
	public void setTheme(String theme) { this.theme = theme; }
	
	public Set<TravauxPratiques> getFormationTps() { return formationTps; }
	public void setFormationTps(Set<TravauxPratiques> formationTps) { this.formationTps = formationTps; } 
	
	public FormationDetail getFormationDetail() { return formationDetail; }
	public void setFormationDetail(FormationDetail formationDetail) { this.formationDetail = formationDetail; } 
		
}
