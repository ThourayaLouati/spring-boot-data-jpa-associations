package tn.esprit.spring.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_TP")
public class TravauxPratiques implements Serializable {
 
	private static final long serialVersionUID = 1L;
  
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name="TP_ID")
	Long tpId;  
 
	@Column(name="TP_SUJET")
	String tpSujet; 
 
	@Column(name="TP_DUREE")
	Long tpDuree; 
 
	@ManyToMany(mappedBy="formationTps", cascade = CascadeType.ALL) 
	private Set<Formation> formations; 
	  
	// Unidirectionnelle : 
//	@OneToMany/*(cascade = CascadeType.ALL)*/
//	@JoinTable(name="T_TP_TC", joinColumns={@JoinColumn(name="TP_ID")}, inverseJoinColumns={@JoinColumn(name="TC_ID")})
//	private Set<TpCorrection> TpCorrections;

	// Bidirectionnelle : 
//	@OneToMany(mappedBy="travauxPratiques")
//	private ArrayList<TpCorrection> TpCorrections;
	 
	public TravauxPratiques() {}
	public TravauxPratiques(String tpSujet, Long tpDuree/*, Set<TpCorrection> tpCorrections*/) {
		this.tpSujet = tpSujet; 
		this.tpDuree = tpDuree;	
		//this.TpCorrections = tpCorrections;
	} 
  
	public Long getTpId() {	return tpId; }
	public void setTpId(Long tpId) { this.tpId = tpId; }
	 
	public String getTpSujet() { return tpSujet; }
	public void setTpSujet(String tpSujet) { this.tpSujet = tpSujet; }
	 
	public Long getTpDuree() { return tpDuree; }
	public void setTpDuree(Long tpDuree) { this.tpDuree = tpDuree; }
	public Set<Formation> getFormations() { return formations; }
	public void setFormations(Set<Formation> formations) { this.formations = formations; }
	  
//	public Set<TpCorrection> getTpCorrections() { return TpCorrections;}
//	public void setTpCorrections(Set<TpCorrection> tpCorrections) {
//		TpCorrections = tpCorrections; }
  
}
 
// if Set is not working, then use List / ArrayList : 
// private List<TravauxPratiques> formationTps;	
